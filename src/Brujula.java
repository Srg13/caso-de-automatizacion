public class Brujula{
    // 1 : norte ^ hacía arriba (+)
    // 2 : sur v hacía abajo (-)
    public  int getNorthSouth(int [] input){
        int value = 0;
        for (int i = 0; i < input.length; i++){
            if(input[i] == 1){
                value++; //para caminar hacia el norte
            }else if(input[i] == 2){
                value--; //para caminar hacia el sur
            }else{
                continue;
            }
        }
        return value;
    }
    // 3 : este -> hacía la derecha (+)
    // 4 : oeste <- hacía la izquierda (-)
    public int getEastWest (int [] input){
        int value = 0;
        for (int i = 0; i < input.length; i++){
            if(input[i] == 3){
                value++; //para caminar hacia el este
            }else if(input[i] == 4){
                value--; //para caminar hacia el oeste
            }else{
                continue;
            }
        }
        return value;
    }

    //Se podrá caminar hacía los cuatro polos cardinales
    public String caminar (int [] input){
        int northSouth = this.getNorthSouth(input);
        int eastWest = this.getEastWest(input);

        String salida = "";

        if(northSouth > 0){
            salida = salida + northSouth + "N";
        }else if(northSouth < 0){
            salida = salida + -northSouth + "S";
        }

        if(eastWest > 0) {
            salida = salida + eastWest + "E";
        }else if(eastWest < 0){
            salida = salida + -eastWest + "O";
        }
        return salida;
    }
}
