import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BrujulaTest {
    private Brujula brujula;
    private int[] array1, array2, array3;

    @Test
    void getNorthSouthTestCero() {
        this.brujula = new Brujula();
        this.array1 = new int[] {1,1,1,2,2,2,2,1};
        assertEquals(0, this.brujula.getNorthSouth(this.array1));
    }
    @Test
    void getNorthSouthTestPositivo() {
        this.brujula = new Brujula();
        this.array1 = new int[] {1,1,1,2,1,3,4,1};
        assertEquals(4, this.brujula.getNorthSouth(this.array1));
    }
    @Test
    void getNorthSouthTestNegativo() {
        this.brujula = new Brujula();
        this.array1 = new int[] {1,2,2,2,1,3,2,1};
        assertEquals(-1, this.brujula.getNorthSouth(this.array1));
    }

    @Test
    void getEastWestTestCero() {
        this.brujula = new Brujula();
        this.array2 = new int[] {3,3,1,4,4,4,2,3};
        assertEquals(0, this.brujula.getEastWest(this.array2));
    }
    @Test
    void getEastWestTestPositivo() {
        this.brujula = new Brujula();
        this.array2 = new int[] {1,2,4,2,3,1,3,3,4};
        assertEquals(1, this.brujula.getEastWest(this.array2));
    }
    @Test
    void getEastWestTestNegativo() {
        this.brujula = new Brujula();
        this.array2 = new int[] {1,2,4,4,4,4,3,3,4};
        assertEquals(-3, this.brujula.getEastWest(this.array2));
    }
    @Test
    void caminarTestNorteOeste() {
        this.brujula = new Brujula();
        this.array3 = new int[] {1,1,1,2,1,4,4,4,4,3,3,4};
        assertEquals("3N3O", this.brujula.caminar(this.array3));
    }
    @Test
    void caminarTestSur() {
        this.brujula = new Brujula();
        this.array3 = new int[] {2,2,2,2,4,3};
        assertEquals("4S", this.brujula.caminar(this.array3));
    }@Test
    void caminarTestSurEste() {
        this.brujula = new Brujula();
        this.array3 = new int[] {2,2,2,2,1,2,3,3,3,3,3,4};
        assertEquals("4S4E", this.brujula.caminar(this.array3));
    }@Test
    void caminarTestOeste() {
        this.brujula = new Brujula();
        this.array3 = new int[] {4,4,3,4,4,3};
        assertEquals("2O", this.brujula.caminar(this.array3));
    }
}